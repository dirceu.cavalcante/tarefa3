import java.util.Scanner;
public class Tarefa2 {

    public static void main (String[] args){
        
        int opcao;
        do{
            System.out.println("\n----------");
            System.out.println("Menu Simples");
            System.out.println("1 - Opção 1");
            System.out.println("2 - Opção 2");
            System.out.println("3 - Sair");
            System.out.print("Digite a opção: ");
            Scanner scanner = new Scanner(System.in);
            opcao = scanner.nextInt();

            if (opcao == 1){
                System.out.println("Você escolheu a primeira opção.");
            } else
            if (opcao == 2){
                System.out.println("Você escolheu a segunda opção.");
            } else
            if (opcao == 3){
                System.out.println("O programa foi encerrado");
            }
            else {
                System.out.println("Opção inválida!");
            }
            
        }while (opcao != 3);        
       
    
    }
}
